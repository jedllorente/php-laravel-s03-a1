<?php

$buildingObj = (object)[
    'name'=>'Caswynn Building',
    'floors'=> 8,
    'address'=>(object)[
        'barangay'=>'Sacred Heart',
        'city'=>'Quezon City',
        'country'=>'Philippines'
    ]
];

// Objects from Classes
class Building {
    public $name;
    public $floors;
    public $address;

    // Constructor is used during the creation of an object.

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function printName() {
        return "The name of the building is $this->name";
    }
}

// extends - used to relate to a certain parent class.
class Condominium extends Building {

}
$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');