<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Objects from Variables</h1>
    <p><?= $buildingObj->address->city;?></p>

    <h1>Objects and Classes</h1>

    <p><?php var_dump($building)?></p>

    <p><?=$building->printName();?></p>

    <h1>Inheritance (Condominium Object)</h1>

    <p><?=$condominium->name;?></p>

    <p><?= $condominium->floors?></p>
    <p><?= $condominium->address?></p>

    <p><?php var_dump($condominium);?></p>

    <!-- Polymorphism -->
    <h1>Polymorphism (Changing of printName Behavior)</h1>
    <p><?= $condominium->printName();?></p>
</body>

</html>